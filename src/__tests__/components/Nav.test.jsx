import React from "react";
import {  unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Nav from '../../components/Nav';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';


let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});



it('should render ', () => {
	act(()=>{
		render(<Nav/>, container);
	});
	
	// const button = container.querySelector("[data-testid='logout']");
	// // const button = container.getByRole('button', {name: /log out/i});
	// expect(button.textContent).toBe("Log Out");
	
	// const menu1 = container.queryByText('Your Financial Plan');
	
})
it('renders button correctly', () => {
	act(()=>{
		render(<Nav/>, container);
	});
	const button = screen.getByTestId( 'logout');
	expect(button).toHaveTextContent('Log Out');
});




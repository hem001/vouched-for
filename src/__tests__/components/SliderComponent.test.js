import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { json_data } from "../../data/data";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import SliderComponent from "../../components/SliderComponent";

window.matchMedia =
	window.matchMedia ||
	function () {
		return {
			matches: false,
			addListener: function () {},
			removeListener: function () {},
		};
	};
let container = null;
beforeEach(() => {
	// setup a DOM element as a render target
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	// cleanup on exiting
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

import { convertToPercentage } from "../../components/SliderComponent";

test("should convert to percentage", () => {
	expect(convertToPercentage(5, 100)).toBe(5);
});

const tdata = { amount: 100, name: "Hello" };

test("should check tooltip value", () => {
	act(() => {
		render(<SliderComponent data={tdata} max={100} />, container);
	});

	const tooltip = screen.getByTestId("slider-tooltip-Hello");
	// expect(input).toHaveValue(tdata.amount);
	expect(tooltip).toHaveTextContent(tdata.amount);
});

test("should check input value", () => {
	const { getByTestId } = render(<SliderComponent data={tdata} max={100} />);
	const input = getByTestId("input-Hello");
	expect(input).toHaveValue(String(tdata.amount));
});

const val = 20;
const name = "help me";
const hdata = { amount: val, name: name };

test("should check input value for null", () => {
	const { getByTestId } = render(<SliderComponent data={hdata} max={100} />);

	const input = getByTestId("input-" + name);
	expect(input).toHaveValue(String(val));
});


 	
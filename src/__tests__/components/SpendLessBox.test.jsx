import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import SpendLessBox from '../../components/SpendLessBox';
import {json_data} from '../../data/data';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('should contain title ', () => {
	act(()=>{
		render( <SpendLessBox 
		 />, container );
	});
	
	const title = screen.getByRole('h2', {name:/spend less/i});
		
})


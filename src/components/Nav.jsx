import React from "react";

import { Menu, Button } from "antd";
import { Link } from "react-router-dom";
import "../styles/nav.css";

export default function Nav() {
    return (
            <div className=" ">
                <Menu
                    mode="horizontal"
                    className="nav-bar"
                >
                    <Menu.Item style={{ float: "left" }} key={"1"}>
                    <Link to='/'>
                        
                        Your Financial Plan
                    </Link>
                    </Menu.Item>
                    
                    
                    <Menu.Item>
                    <Link to='/form'>
                    Form
                    </Link>

                    </Menu.Item>
                    
                    <Menu.Item>
                    <Link to='/form-new'>
                    Form-New
                    </Link>

                    </Menu.Item>

                    <Menu.Item style={{ float: "right" }}>
                        <Button type="default" data-testid="logout" name='logout'>
                            Log Out
                        </Button>
                    </Menu.Item>
                </Menu>
            </div>
    )
}



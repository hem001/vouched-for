import React from "react";
import styled from "styled-components";
import Counter from "@livestd/react-render-counter";

const MyDiv = styled.div`
	float: right;
	min-width:15px;
	text-align:center;
	background-color: yellow;
	font-weight:bold;
	border-radius:35%;
	border: 1px solid ;
`;
export default function RenderCounter() {
	return (
		<MyDiv>
			<Counter />
		</MyDiv>
	);
}

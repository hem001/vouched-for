import React, { useState, useContext } from "react";
import {StateContext} from '../App';

import SliderComponent from "./SliderComponent";
import { message } from "antd";
import "../styles/app.css";
import thumbs_up from "../data/thumbs_up.png";
import thumbs_down from "../data/thumbs_down.png";

import { Button, Row, Col } from "antd";

export default function SpendLessBox(props) {
   const [thumb_clicked, setThumbClicked] = useState(false);
   const state = useContext(StateContext);

   function handleThumbsclick(msg) {
      setThumbClicked(true);
      message.success(msg);
   }
   return (
      <div>
         <p align="center">
            Try reducing your monthly spending to see how your forecast could
            improve!
         </p>
         {state.data.expenditures.map((i, j) => (
            <SliderComponent
               key={j}
               index ={j}
               data={i}
               max={i.amount}
            />
         ))}

         <div>
            <strong>
               This means you are saving{" "}
               <span style={{ color: "lightgreen" }}>{state.savings}</span> per
               month.
            </strong>
         </div>
         <div className="button-style">
            <Button
               block
               href="http://www.google.com"
               target="_blank"
               data-testid="find_ways_to_save"
            >
               Find ways to save
            </Button>
         </div>

         {!thumb_clicked ? (
            <Row>
               <Col sm={10}></Col>
               <Col sm={8}>
                  {" "}
                  <span style={{ textDecoration: "underline" }}>
                     Was this helpful?
                  </span>{" "}
               </Col>
               <Col sm={6}>
                  <img
                     data-testid="thumbs_up"
                     height="25px"
                     width="25px"
                     src={thumbs_up}
                     alt="thumbs up"
                     onClick={() => handleThumbsclick("You have clicked like")}
                  />
                  <span style={{ padding: "5px" }}></span>

                  <img
                     data-testid="thumbs_down"
                     height="25px"
                     width="25px"
                     src={thumbs_down}
                     alt="thumbs down"
                     onClick={() => {
                        handleThumbsclick("You have clicked dislike");
                     }}
                  />
               </Col>
            </Row>
         ) : null}
      </div>
   );
}

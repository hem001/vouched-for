import React,{useState, useContext} from "react";
import {StateContext} from '../App';

import InputComponent from "./InputComponent";

import "../styles/app.css";

export default function IncomeAndSpend() {
	const state = useContext(StateContext);
	
	return (
		<div>
			<strong>Annual income</strong>
			{state.data.incomes.map((i, j) => (
				<InputComponent
					key={j}
					index={j}
					data={i}
					type={"incomes"}
				/>
			))}

			<strong>Monthly spending</strong>
			{state.data.expenditures.map((i, j) => (
				<InputComponent
					key={j}
					index={j}
					data={i}
					type={"expenditures"}
				/>
			))}
		</div>
	)
}

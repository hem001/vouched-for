import React from "react";
import ContentCardBox from "./ContentCardBox";
import { Form, Field, FormSpy } from "react-final-form";
import styled from "styled-components";
import { Row, Col } from "antd";
import RenderCounter from "./RenderCounter";

const Mydiv = styled.div`
	width: 60%;
	max-width: 600px;
	margin: auto;
	margin-top: 50px;
`;

const StyledDiv = styled.div`
	padding: 5px;
	margin: 5px;
`;

const PreDiv = styled.div`
	min-height: 80px;
	background-color: lightgray;
`;

export default function ReactFinalForm() {
	const onSubmit = () => {
		console.log("submitted ");
	};

	return (
		<Mydiv>
			<ContentCardBox title={"React Final Form"}>
				<h2 align="center">Hello</h2>
				<Form
					onSubmit={onSubmit}
					subscription={{
						submitting: true,
					}}
				>
					{({ handleSubmit, values, submitting, pristine, form }) => (
						<form onSubmit={handleSubmit}>
						<RenderCounter /><br/><br/>
							<Field name="firstName" subscription={{
								value:true,touched:true
							}} >
								{({ input }) => (
									<div className="">
									<RenderCounter />
										<Row>
											<Col sm={8}>
												<label htmlFor="">First Name:</label>
											</Col>
											<Col sm={16}>
												<input {...input} placeholder="First Name" />
											</Col>
										</Row>
									</div>
								)}
							</Field>
							<Field name="lastName" subscription={{
								value:true,touched:true
							}}>
								{({ input }) => (
									<div className="">
										<RenderCounter />
										<Row>
											<Col sm={8}>
												<label htmlFor="">Last Name:</label>
											</Col>
											<Col sm={16}>
												<input {...input} placeholder="Last Name" />
											</Col>
										</Row>
									</div>
								)}
							</Field>
							<Row>
								<Col sm={12}>
									<button type="submit" disabled={submitting || pristine}>
										Submit
									</button>
								</Col>
								<Col sm={12}>
									<button
										type="button"
										onClick={form.reset}
										disabled={submitting || pristine}
									>
										Reset
									</button>
								</Col>
							</Row>

							<PreDiv>
								<FormSpy subscription={{ values: true }}>
									{({ values }) => (
										<pre>
										<RenderCounter />
											{JSON.stringify(values, 0, 2)}
										</pre>
									)}
								</FormSpy>
							</PreDiv>
						</form>
					)}
				</Form>
			</ContentCardBox>
		</Mydiv>
	);
}

import React, {  useState, useContext } from "react";
import { Row, Col } from "antd";
import {DispatchContext} from '../App';
import "../styles/slider-component.css";

export function convertToPercentage(val, max) {
	return Math.ceil((val / max) * 100);
}


export default function SliderComponent(props) {
	const [data, setData] = useState(props.data);
	const [max, setMax] = useState(props.max);
	
 	const dispatch = useContext(DispatchContext);
 	
 	 	
	let percentage = convertToPercentage(
		data.amount,
		max
	);
	let left_offset =(percentage>100?80:
		percentage < 20 ? percentage : percentage - Math.ceil(percentage / 5));
		
	function onSliderChange(e) {
		const { value } = e.target;
		const index = props.index;
		let new_data = data;
		new_data.amount = value;
		// setData(new_data);
		dispatch({
			type:'update',
			payload: {index,new_data },
			incomeOrExpenditure:'expenditures'
		});

	}
	return (
		<div>
			<Row style={{ paddingBottom: "30px" }}>
				<Col xs={24} sm={12}>
					<strong>{data.name}</strong>{" "}
				</Col>

				<Col xs={24} sm={12}>
					<div className="range">
						<div className="range1">
							<span
								data-testid={"slider-tooltip-" + data.name}
								className="tooltip"
								style={{ left: left_offset + "%" }}
							>
								£{data.amount}{" "}
							</span>
						</div>
						<input
							data-testid={"input-" + data.name}
							type="range"
							min="0"
							max={max}
							value={data.amount}
							name={data.name}
							className="slider"
							onChange={onSliderChange}
						/>
					</div>
				</Col>
			</Row>
		</div>
	)
}


import React, { useState, useContext } from "react";
import "../styles/app.css";
import { Input, Row, Col } from "antd";
import {StateContext, DispatchContext} from '../App';

export default function InputComponent(props) {
	const [data, setData] = useState(props.data);
	
	const state = useContext(StateContext);
	const dispatch = useContext(DispatchContext);
	
	
	function handleChange(e) {
		
		const { name, value } = e.target;

		let new_data = data;
			new_data[name] = value;
		const index = props.index;
		const type = props.type;
		
		
		dispatch({type:'update', 
		         payload:{index,new_data},
		         	incomeOrExpenditure: type
		         }         
		         );
		
	}
	return (
		<Row>
			<Col md={12} sm={24} className="col-style">
				{data.name}:
				<Input
					data-testid={"spend-" + data.name}
					type="number"
					value={props.data.amount}
					name="amount"
					onChange={handleChange}
				/>
			</Col>

			<Col md={6} sm={24} className="col-style">
				From age:
				<Input
					type="number"
					value={data.from_age}
					name="from_age"
					onChange={handleChange}
				/>
			</Col>

			<Col md={6} sm={24} className="col-style">
				To age:
				<Input
					type="number"
					value={data.to_age}
					name="to_age"
					onChange={handleChange}
				/>
			</Col>
		</Row>
	);
}

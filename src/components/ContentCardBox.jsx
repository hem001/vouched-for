import React from "react";
import styled from "styled-components";
const Card = styled.div`
	width: 95%;
	min-height: 400px;
	border: solid 2px teal;
	box-shadow: 5px 5px lightgray;
	
`;
const CardHeader = styled.div`
	text-align: center;
	background-color: teal;
	height: 50px;
	padding: 10px;
	margin:0px;
`;
const CardContent = styled.div`
	padding: 20px;
`;
export default function ContentCardBox(props) {
	

	return (
		<React.Fragment>
			<Card>
				<CardHeader>
					<h2 style={{ color: "white" }}>{props.title}</h2>
				</CardHeader>
				<CardContent>
					{props.children}
				</CardContent>
			</Card>
		</React.Fragment>
	);
}

import React from 'react';
import ReactDOM from 'react-dom';
import {RouterComponent} from './RouterComponent';

ReactDOM.render(
  <RouterComponent />
  ,document.getElementById('root')
);

import React, { useReducer } from "react";

import IncomeAndSpend from "./components/IncomeAndSpend";
import SpendLessBox from "./components/SpendLessBox";
import ContentCardBox from "./components/ContentCardBox";

import "./styles/app.css";
import { json_data } from "./data/data.js";

export const StateContext = React.createContext();
export const DispatchContext = React.createContext();

export default function App() {
  const [state, dispatch] = useReducer(reducer, null, getInitialData);

  function reducer(state, action) {
    switch (action.type) {
      case "update":
        let ret = Object.assign({}, state.data);
        const type = action.incomeOrExpenditure;
        ret[type][action.payload.index] = action.payload.new_data;
        const savings = getSaving(ret);


        //store data in session storage
        sessionStorage.setItem("myData", JSON.stringify(ret));
        sessionStorage.setItem("savings", savings);

        // return state;

      return Object.assign({}, state,{
        data: ret,
        savings: savings,
      } )

      default:
        throw new Error();
    }
  }

  return (

      <div className="app">
        <DispatchContext.Provider value={dispatch}>
          <StateContext.Provider value={state}>
            <ContentCardBox title="INCOME AND SPEND">
              <IncomeAndSpend />
            </ContentCardBox>

<ContentCardBox title="SPEND LESS"             children={<SpendLessBox />} />
          </StateContext.Provider>
        </DispatchContext.Provider>
      </div>
  );
}

function getSaving(data) {
  var tot_expenditures = 0;
  for (let i = 0; i < data.expenditures.length; i++) {
    tot_expenditures += data.expenditures[i].amount * 12;
  }
  return Math.round((data.incomes[0].amount - tot_expenditures) / 12);
}

function getInitialData() {
  let data;
  if (!sessionStorage.getItem("myData")) {
    sessionStorage.setItem("myData", JSON.stringify(json_data));
    data = json_data;
  } else {
    data = JSON.parse(sessionStorage.myData);
  }

  const savings = getSaving(data);
  sessionStorage.setItem("savings", savings);
  return { data: data, savings: savings };
}

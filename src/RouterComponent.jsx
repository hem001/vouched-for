import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "antd/dist/antd.css";
import Nav from "./components/Nav";
import App from "./App";
import ReactFinalForm from "./components/ReactFinalForm";
import ReactFinalFormNew from "./components/ReactFinalFormNew";

class RouterComponent extends Component {
    render() {
        return (
            <div>
                <Router>
                    <Nav />
                    <Switch>
                        <Route exact path="/" component={App} />
                        <Route path='/form' component={ReactFinalForm} />
                        <Route path='/form-new' component={ReactFinalFormNew} />
                    </Switch>
                </Router>
            </div>
        );
    }
}
export { RouterComponent };

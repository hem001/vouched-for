describe("Home Page", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should find YOUR INCOME & SPEND", () => {
    cy.contains("h2", "YOUR INCOME & SPEND");
  });
  it("should find SPEND LESS", () => {
    cy.contains("h2", "SPEND LESS");
  });

  it("clicking thumbs up should show message and option shuold disappear", () => {
    cy.get("[data-testid=thumbs_up]").click();
    cy.get(".ant-message").should("contain", "You have clicked like");
    cy.get("[data-testid=thumbs_up]").should("not.exist");
  });

  it("clicking thumbs down should show message and option should disappear", () => {
    cy.get("[data-testid=thumbs_down]").click();
    cy.get(".ant-message").should("contain", "You have clicked dislike");
    cy.get("[data-testid=thumbs_down]").should("not.exist");
  });

  it("clicking Find Ways to Save Button should open new window ", () => {
    cy.get("[data-testid=find_ways_to_save]").click();
  });

  it("changing value in Mortgage input should change slider tooltip value", () => {
    var temp = 1100;
    cy.get("[data-testid=spend-Mortgage]").clear().type(temp);
    cy.get("[data-testid=slider-tooltip-Mortgage]").contains(temp);
    cy.get("[data-testid=input-Mortgage]").should("have.value", temp);
  });

  it("changing value in Bills input should change slider tooltip value", () => {
    var temp = 1100;
    cy.get("[data-testid=spend-Bills]").clear().type(temp);
    cy.get("[data-testid=slider-tooltip-Bills]").contains(temp);
    // cy.get('[data-testid=input-Bills]').should('have.value',temp);
  });

  it("should should retain input value after window refresh", () => {
    var temp = 1000;
    cy.get("[data-testid=spend-Bills]").clear().type(temp);
    cy.reload();
    cy.get("[data-testid=spend-Bills]").should("have.value", temp);
    cy.get("[data-testid=slider-tooltip-Bills]").contains(temp);
    cy.get("[data-testid=input-Bills]").should("have.value", temp);
  });
});
